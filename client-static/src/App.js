import React from 'react'
import { Root, Routes, addPrefetchExcludes } from 'react-static'

import { Router } from '@reach/router'
import { ApolloProvider } from '@apollo/react-hooks'
// import { ApolloProvider as ApolloHooksProvider } from 'react-apollo-hooks'


import client from './library/connectors/apollo'

import Dynamic from './library/components/Dynamic'
import Nav from './library/components/Nav/Nav'  
import Loader from './library/components/Loader'
import './app.css'

// Any routes that start with 'dynamic' will be treated as non-static routes
// addPrefetchExcludes(['dynamic'])

function App() {
  return (
    <ApolloProvider client={client}>
        <Root>
          <Nav/>
          <div className="content">
            <React.Suspense fallback={<Loader/>}>
              <Router>
                <Dynamic path="dynamic" />
                <Routes path="*" />
              </Router>
            </React.Suspense>
          </div>
        </Root>
    </ApolloProvider>
  )
}

export default App
