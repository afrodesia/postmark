import React, { Fragment } from 'react'
import { Helmet } from 'react-helmet'

import Home from '../library/pages/Home/'

export default () => {

    return (
      <Fragment>
        <Helmet>
        	<title> Home - UI Components </title>
          <meta property="og:title" content="Home" />
          <meta name="description" content="This is the Home section" />
      	</Helmet>
   
          <Home /> 

      </Fragment>
    )

}