import React, { Fragment } from 'react'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'
const AboutBg = styled.div`
    width:100%;
    height:15.6vmin;
    background:lightgreen;
    h1{
      color:#fff;
      
    }
`
export default () => (
  <Fragment>
    <Helmet>
      <title> About - UI Components </title>
      <meta property="og:title" content="About" />
      <meta name="description" content="This is the About section" />
    </Helmet>
    <AboutBg className="About">
      <h1 className="container">About</h1>
    </AboutBg>
  </Fragment>
)
