import React, { Fragment } from 'react'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'
const RegisterBg = styled.div`
    width:100%;
    height:15.6vmin;
    background:lightgreen;
    h1{
      color:#fff;
      
    }
`
export default () => (
  <Fragment>
    <Helmet>
      <title> Register - UI Components </title>
      <meta property="og:title" content="About" />
      <meta name="description" content="This is the  section" />
    </Helmet>
    <RegisterBg className="About">
      <h1 className="container">Register</h1>
    </RegisterBg>
  </Fragment>
)
