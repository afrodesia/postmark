import React from 'react'
import { useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'

const FETCH_POST_QUERY = gql`
  {
    getPosts{
      id username body likeCount
      likes{
        username
      }
      commentCount
      comments{
        id username createdAt body
      }
    }
  }
`
export default function GetPost() {

  const { 
    loading, 
    data: { getPosts: posts}
    } = useQuery(FETCH_POST_QUERY)
  if(posts){
    console.log(posts)
  }
  return (
    <div>
      <h3>Recent posts</h3>
      {/*    */}
        {loading ? (
            <p>loading posts..</p>
         ) : (
           posts && JSON.stringify(posts, null,  2)

        )}
     
    </div>
  )
}

// function PostCard({ post: { body, createdAt, id, username }}) {

//   console.log(post)

//   return (
//     <div>
//       {body}
//       {username}
//     </div>
//   )
// }

