import React, { Fragment } from 'react'
// import { addPrefetchExcludes } from 'react-static'

import Masthead from './Masthead.js'
import GetPost from './GetPost'

export default function Home() {
  return (
    <Fragment>
        <Masthead/>
        <GetPost/>
    </Fragment>
  )
}


