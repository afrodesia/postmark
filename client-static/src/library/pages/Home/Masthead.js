import React from 'react';
import styled from 'styled-components'

//  Styles 
const MastheadBg = styled.div`
  width:100%;
  min-height:40.6vmin;
  background:lightgreen;
  display:flex;
  align-items: center;
  justify-content: center;

  div{
    max-width: 50%;
    color:#fcfcf7;
  }
  h1{
    font-weight:900;
    color:#fcfcf7;
    i{
      padding:10px 20px 0px 0px;
      
    }
  }
  p{
    padding-top:20px;
    text-align:center;
    clear:both;
    font-size:1.2em;
    font-weight:800;
    
  }
  @media only screen and (max-width: 800px){
    height:100vh;
    div{
      max-width: 80%;
    }
    h1{
      font-size:2em;
      text-align:center;
    }
  }
`;

const Masthead = () => {
  return (
    <MastheadBg>
      <div>
        <h1> <i className='fa fa-envelope'></i>postmark</h1>
        <p>"Post what maters to you, make your mark!"</p>  
      </div>
    </MastheadBg>
  );
};

export default Masthead;