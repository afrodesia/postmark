// import  React from 'react'

import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloProvider } from '@apollo/react-hooks'
import fetch from 'node-fetch'

const client = new ApolloClient({
  ssrMode: true,
  link: new HttpLink({
    fetch,
    uri: 'http://localhost:3001/graphql',
    // credentials: 'same-origin',
  }),
  cache: new InMemoryCache(),
  ssrForceFetchDelay: 100,
})

export default client