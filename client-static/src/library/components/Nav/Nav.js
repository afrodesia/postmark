import React, { Component } from 'react'
import { Link  } from '@reach/router'
import styled from 'styled-components'


// APP STYLES
// import './Nav.scss'

const Header = styled.header`
  width: 100%;
  height: 80px;
  position: relative;
  color:#222;
  z-index: 3;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0px 100px 0;
  background-color: rgba(255, 255, 255, 0.1);
  background: rgba(255, 255, 255, 0.1);
  color: rgba(255, 255, 255, 0.1);
  background:#fcfcf7;

  -webkit-box-shadow: -2px 9px 15px -8px rgba(0,0,0,0.36);
  -moz-box-shadow: -2px 9px 15px -8px rgba(0,0,0,0.36);
  box-shadow: -2px 9px 15px -8px rgba(0,0,0,0.36);
  .logo{
    margin-top: 0px;
  }
  .logo a{
    
    background: lightgreen;
    display: inline-block;
    width: auto !important;
    /* padding: 10px 10px; */
    border-radius: 16px;
    /* font-size: 1.4rem; */
    font-size: 1.6rem;
    font-weight:900;
    text-decoration:none;
    color:#fcfcf7 !important;
    cursor: pointer;
    .active{
      font-size: 1.6rem;
      
      
    }
    span{
        padding-left:10px;
        font-weight:900;
    }
  }
  .gray{
    color:#ccc;
  }
  a{
    color:#222;
    /* opacity: 0.55; */
    /* transition:all .6s; */
    padding: 8px 10px;
    cursor: pointer;
  }
  a:hover{
    opacity: 1;
    color:lightgreen;
  }
  [aria-current],
  a.active{
    color:#222;
    color:lightgreen !important;
    /* font-family:'ClanPro-Bold'; */
    background: #5b5b5b;
    padding: 8px 10px;
    border-radius: 14px;
  } 
  .fa-bars{
    display: none;
    color:lightgreen;
    font-size: 2rem;
    &:hover{
      color:lightgreen;
    }
  }
  nav {
    ul{
      display: flex;
      justify-content: space-between; 
    }
    li{
       margin: 0 15px;
       justify-content: space-between;
       a{
         text-decoration: none;
       }
      a.active{
        color:#fff;
        &:hover{
          color:#222;
        }
      } 
    }
  }

  @media only screen and (max-width: 800px){
    padding: 0px;
      .logo{
        padding-left: 15px;
      padding-top:0px !important;
      }
  }
  @media only screen and (max-width: 768px){
     height: auto;
     min-height: 50px;
     display: block;
     position: relative;
     padding-top:9px;
     .logo{
       margin-top: 20px;
       margin: 0px;
       margin-left: -5px;
       margin-bottom:10px; 
     }
     .fa-bars{
       display: inline-block;
       position: absolute;
       top:10px;
       right:10px;
       cursor: pointer;
     }
     ul.collapsed{
      width: 100%;
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      flex-wrap: wrap;

      overflow: hidden;
      max-height: 0;
      -moz-transition-duration: 0.4s;
      -webkit-transition-duration: 0.4s;
      -o-transition-duration: 0.4s;
      transition-duration: 0.4s;
      -moz-transition-timing-function: cubic-bezier(0, 1, 0.5, 1);
      -webkit-transition-timing-function: cubic-bezier(0, 1, 0.5, 1);
      -o-transition-timing-function: cubic-bezier(0, 1, 0.5, 1);
      transition-timing-function: cubic-bezier(0, 1, 0.5, 1);

      &.is-expanded{
        overflow: hidden;
        max-height: 500px; /* approximate max height */
        -moz-transition-duration: 0.4s;
        -webkit-transition-duration: 0.4s;
        -o-transition-duration: 0.4s;
        transition-duration: 0.4s;
        -moz-transition-timing-function: ease-in;
        -webkit-transition-timing-function: ease-in;
        -o-transition-timing-function: ease-in;
        transition-timing-function: ease-in;
      } 
      li{
        padding: 15px 10px;
        margin:0px 0px;
        width: 100%;
      } 
    } 
  }  
`;


class Nav extends Component {
  constructor(props){
    super(props)
    this.state = {
      isExpanded : false
    }
  }
  handleToggle(e){
    e.preventDefault()
    this.setState({
      isExpanded: !this.state.isExpanded
    })
  }
  render(){

    const  { isExpanded } = this.state
    const closeMenu = () =>
      this.setState({
        isExpanded: false
     })
    
    return(

    <Header>
      <div className="logo">
        <Link to="/" onClick={closeMenu}>
          <i className='fa fa-envelope-o'></i>
          <span>postmark</span> 
        </Link>
      </div>
      <nav className="nav">
        <i className="fa fa-bars" aria-hidden="true" onClick={(e) => this.handleToggle(e)}>
          </i>
        <ul className={`collapsed ${isExpanded ?  'is-expanded' : '' }`}>
          <li><Link to="/about" onClick={closeMenu} >about</Link></li>
          <li><Link to="/login" onClick={closeMenu} >login</Link></li>
          <li><Link to="/register" onClick={closeMenu} >register</Link></li>

        </ul>
      </nav>
    </Header>  
  )
 }
}
export default Nav
