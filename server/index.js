// import cors from 'cors';
import express from 'express';
import { ApolloServer,  PubSub } from 'apollo-server-express'
import mongoose from 'mongoose'
import schema from './api/v2/graphql/typeDefs'
import resolvers from './api/v2/graphql/resolvers'

const app = express();

// Database Connection
const MONGO_URI = 'mongodb://localhost/postmark'
mongoose.connect(MONGO_URI, { useNewUrlParser: true }, function(err, res){
  if (err){
    console.log('DB CONNECTION FAILED')
  }
  else {
    console.log('DB CONNECTION ESTABLISHED!')
  }
})

const pubsub = new PubSub();

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  context: ({ req }) => ({ req, pubsub }),
  playground: {
    settings: {
      'editor.theme': 'light',
    }
  },
});

server.applyMiddleware({ app, path: '/graphql' });

app.listen({ port: 3001 }, () => {
  console.log('Apollo Server on http://localhost:3001/graphql');
});
