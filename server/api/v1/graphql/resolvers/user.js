const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
// const { UserInputError } = require('apollo-server');
// import { SECRET_KEY } from '../config/config.js'
const { SECRET_KEY} = require('../config/config.js')
const User = require('../models/User')

module.exports = {
  Mutation : {
    async register(_, 
      {
      registerInput: username, email, password, confirmPassword 
      },
    ){
      password = await bcrypt.hash(password, 10)

      const newUser = new User({
        email,
        username,
        password,
        createdAt: new Date().toISOString()
      })

      const res = await newUser.save()

      const token = jwt.sign({
        id: res.id,
        email: res.email,
        username: res.username
      },SECRET_KEY, { expiresIn: '1h'})
      return{
        ...res._doc,
        id:res._id,
        token
      }
    }
  }
}


// export default usersResolver