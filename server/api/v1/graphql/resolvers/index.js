import postsResolvers from './post'
import usersResolvers from './user'

const resolvers = {
  Query: {
    ...postsResolvers.Query
  },
  Mutation: {
    ...usersResolvers.Mutation
  }
}

export default resolvers